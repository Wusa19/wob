import { Injectable } from '@angular/core';
import songtexts from "../../data/songtexts";

@Injectable()
export class SongsServiceProvider {

  getSongs() {
    return songtexts;
  }

}
