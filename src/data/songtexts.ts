export default [
  {
    id: "1",
    title: "Oh Tannenbaum",
    text: "O Tannenbaum, o Tannenbaum,\n" +
      "wie treu sind deine Blätter!\n" +
      "Du grünst nicht nur zur Sommerzeit,\n" +
      "nein, auch im Winter, wenn es schneit.\n" +
      "O Tannenbaum,o Tannenbaum,\n" +
      "wie treu sind deine Blätter!\n" +
      "\n" +
      "O Tannenbaum, o Tannenbaum,\n" +
      "du kannst mir sehr gefallen.\n" +
      "Wie oft hat nicht zur Weihnachstzeit\n" +
      "ein Baum von Dir mich hoch erfreut!\n" +
      "O Tannenbaum,o Tannenbaum,\n" +
      "du kannst mir sehr gefallen!\n" +
      "\n" +
      "O Tannenbaum, o Tannenbaum,\n" +
      "dein Kleid will mich was lehren:\n" +
      "Die Hoffnung und Beständigkeit\n" +
      "gibt Kraft und Trost zu jeder Zeit.\n" +
      "O Tannenbaum,o Tannenbaum,\n" +
      "dein Kleid will mich was lehren.",
    icon: ""
  },
  {
    id: "2",
    title: "Oh du Fröhliche",
    text: "O du fröhliche, o du selige,\n" +
      "gnadenbringende Weihnachtszeit!\n" +
      "Welt ging verloren,\n" +
      "Christ ist geboren:\n" +
      "freue, freue dich, o Christenheit!\n" +
      "\n" +
      "O du fröhliche, o du selige,\n" +
      "gnadenbringende Weihnachtszeit!\n" +
      "Christ ist erschienen,\n" +
      "uns zu versühnen:\n" +
      "freue, freue dich, o Christenheit!\n" +
      "\n" +
      "O du fröhliche, o du selige,\n" +
      "gnadenbringende Weihnachtszeit!\n" +
      "Himmlische Heere\n" +
      "jauchzen dir Ehre:\n" +
      "Freue, freue dich o Christenheit!",
    icon: "happy"
  },
  {
    id: '3',
    title: 'Stille Nacht, heilige Nacht',
    text: 'Stille Nacht, heilige Nacht!\n' +
      'Alles schläft, einsam wacht\n' +
      'Nur das traute hochheilige Paar.\n' +
      'Holder Knabe im lockigen Haar,\n' +
      'Schlaf in himmlischer Ruh!\n' +
      'Schlaf in himmlischer Ruh!\n' +
      '\n' +
      'Stille Nacht, heilige Nacht,\n' +
      'Hirten erst kund gemacht!\n' +
      'Durch der Engel Halleluja\n' +
      'tönet es laut von fern und nah:\n' +
      'Christ der Retter ist da!\n' +
      'Christ der Retter ist da!\n' +
      '\n' +
      'Stille Nacht, heilige Nacht,\n' +
      'Gottes Sohn, o wie lacht\n' +
      'Lieb‘ aus Deinem göttlichen Mund,\n' +
      'Da uns schlägt die rettende Stund,\n' +
      'Christ, in Deiner Geburt!\n' +
      'Christ, in Deiner Geburt!',
    icon: ''
  }, {
    id: '4',
    title: 'Alle Jahre wieder',
    text: 'Alle Jahre wieder\n'+
    'kommt das Christuskind.\n'+
    'Auf die Erde nieder,\n'+
    'wo wir Menschen sind.\n'+
    '\n'+
    'Kehrt mit seinem Segen\n'+
    'ein in jedes Haus.\n'+
    'Geht auf allen Wegen\n'+
    'mit uns ein und aus.\n'+
    '\n'+
    'Steht auch mir zur Seite,\n'+
    'still und unerkannt,\n'+
    'dass es treu mich leite\n'+
    'an der lieben Hand.',
    icon: ''
  }, {
    id: '5',
    title: 'Fröhliche Weihnacht überall',
    text: 'Fröhliche Weihnacht überall!\n'+
    'Tönet durch die Lüfte froher Schall\n'+
    '\n'+
    'Weihnachtston, Weihnachtsbaum\n'+
    'Weihnachtsduft in jedem Raum\n'+
    '\n'+
    'Fröhliche Weihnacht überall!\n'+
    'Tönet durch die Lüfte froher Schall\n'+
    '\n'+
    'Darum alle stimmet in den Jubelton,\n'+
    'denn es kommt das Licht der Welt\n'+
    'von des Vaters Thron\n'+
    '\n'+
    'Fröhliche Weihnacht überall!\n'+
    'Tönet durch die Lüfte froher Schall',
    icon: ''
  }, {
    id: '6',
    title: 'Ihr Kinderleich kommet',
    text: 'Ihr Kinderlein kommet,\n'+
    'o kommet doch all\'\n'+
    'zur Krippe her kommet\n'+
    'in Bethlehems Stall,\n'+
    'und seht,\n'+
    'was in dieser hochheiligen Nacht\n'+
    'der Vater im Himmel\n'+
    'für Freude uns macht!\n'+
    '\n'+
    'O seht in der Krippe\n'+
    'im nächtlichen Stall,\n'+
    'seht hier bei des Lichtleins hellglänzendem Strahl\n'+
    'den lieblichen Knaben,\n'+
    'das himmlische Kind,\n'+
    'viel schöner und holder,\n'+
    'als Engel es sind.\n'+
    '\n'+
    'Da liegt es, das Kindlein,\n'+
    'auf Heu und auf Stroh,\n'+
    'Maria und Joseph betrachten es froh.\n'+
    'Die redlichen Hirten\n'+
    'knie\'n betend davor,\n'+
    'hoch oben schwebt\n'+
    'jubeld der Engelein Chor.',
    icon: ''
  }, {
    id: '7',
    title: 'Jingle Bells (Englisch)',
    text: 'Dashing through the snow\n'+
    'in a one-horse open sleigh,\n'+
    'over the fields we go, laughing all the way.\n'+
    'Bells on bobtail ring, making spirits bright\n'+
    'what fun it is to ride\n'+
    'and sing a sleighing song tonight.\n'+
    '\n'+
    'Jingle, bells! Jingle, bells! Jingle all the way!\n'+
    'O what fun it is to ride\n'+
    'in a one-horse open sleigh!\n'+
    'Jingle, bells! Jingle, bells! Jingle all the way!\n'+
    'O what fun it is to ride\n'+
    'in a one-horse open sleigh!\n'+
    '\n'+
    'A day or two ago I thought I\'d take a ride,\n'+
    'and soon Miss Fannie Bright, was seated by my side.\n'+
    'The horse was lean and lank,\n'+
    'misfortune seemed his lot,\n'+
    'he got into a drifted bank\n'+
    'and we, we got upsot.\n'+
      '\n'+
    'Jingle, bells! Jingle, bells! Jingle all the way!\n'+
    'O what fun it is to ride\n'+
    'in a one-horse open sleigh!\n'+
    'Jingle, bells! Jingle, bells! Jingle all the way!\n'+
    'O what fun it is to ride\n'+
    'in a one-horse open sleigh!',
    icon: ''
  }, {
    id: '8',
    title: 'Kling Glöckchen, klingelingeling',
    text: 'Kling Glöckchen, klingelingeling,\n'+
    'kling Glöckchen, kling\n'+
    'Lasst mich ein ihr Kinder,\n'+
    'ist so kalt der Winter,\n'+
    'öffnet mir die Türen,\n'+
    'lasst mich nicht erfrieren!\n'+
    '\n'+
    'Kling Glöckchen, klingelingeling\n'+
    'Kling Glöckchen, klingelingeling\n'+
    '\n'+
    'Mädchen hört und Bübchen,\n'+
    'macht mir auf das Stübchen\n'+
    'bring euch viele Gaben,\n'+
    'sollt euch dran erlaben.\n'+
    '\n'+
    'Kling Glöckchen, klingelingeling\n'+
    'Kling Glöckchen, klingelingeling\n'+
    '\n'+
    'Hell erglühn die Kerzen,\n'+
    'öffnet mir die Herzen!\n'+
    'Will drin wohnen fröhlich,\n'+
    'frommes Kind, wie selig.\n'+
      '\n'+
    'Kling Glöckchen, klingelingeling\n'+
    'Kling Glöckchen, klingelingeling',
    icon: ''
  }, {
    id: '9',
    title: 'Leise rieselt der Schnee',
    text: 'Leise rieselt der Schnee,\n'+
    'still und starr ruht der See,\n'+
    'weihnachtlich glänzet der Wald,\n'+
    'Freue dich, Christkind kommt bald.\n'+
    '\n'+
    'In den Herzen wird\'s warm,\n'+
    'still schweigt Kummer und Harm,\n'+
    'Sorge des Lebens verhallt,\n'+
    'Freue dich, Christkind kommt bald.\n'+
    '\n'+
    'Bald ist heilige Nacht,\n'+
    'Chor der Engel erwacht,\n'+
    'hört nur, wie lieblich es schallt,\n'+
    'Freue dich, Christkind kommt bald.',
    icon: ''
  }, {
    id: '10',
    title: 'Macht hoch die Tür',
    text: 'Macht hoch die Tür, die Tor\' macht weit,\n'+
    'Es kommt der Herr der Herrlichkeit,\n'+
    'Ein König aller Königreich\',\n'+
    'Ein Heiland aller Welt zugleich,\n'+
    'Der Heil und Leben mit sich bringt;\n'+
    'Derhalben jauchzt, mit Freunden singt.\n'+
    'Gelobet sei mein Gott,\n'+
    'Mein Schöpfer, reich von Rat!\n'+
    '\n'+
    'Er ist gerecht, ein Helfer wert,\n'+
    'Sanftmütigkeit ist sein Gefährt,\n'+
    'Sein Königskron\' ist Heiligkeit,\n'+
    'Sein Zepter ist Barmherzigkeit.\n'+
    'All unsre Not zum End\' er bringt.\n'+
    'Derhalben jauchzt, mit Freunden singt.\n'+
    'Gelobet sei mein Gott,\n'+
    'Mein Schöpfer, reich von Rat!',
    icon: ''
  }
]
