import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {MapPage} from "../pages/map/map";
import {SongsPage} from "../pages/songs/songs";
import {SongtextPage} from "../pages/songtext/songtext";
import {ChatPage} from "../pages/chat/chat";
import {ChatroomPage} from "../pages/chatroom/chatroom";

import { AngularFireAuthModule } from 'angularfire2/auth'
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {IonicStorageModule} from "@ionic/storage";
import { Geolocation } from "@ionic-native/geolocation";
import { BackgroundGeolocation } from "@ionic-native/background-geolocation";

var config = {
  apiKey: "AIzaSyA9a6vuCMv0OK4n_rEuHK8ZYUOZcV6ztsc",
  authDomain: "lankemo-wob.firebaseapp.com",
  databaseURL: "https://lankemo-wob.firebaseio.com",
  projectId: "lankemo-wob",
  storageBucket: "lankemo-wob.appspot.com",
  messagingSenderId: "362626688928"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    MapPage,
    SongsPage,
    SongtextPage,
    ChatPage,
    ChatroomPage
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Zurück'
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    MapPage,
    SongsPage,
    SongtextPage,
    ChatPage,
    ChatroomPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    BackgroundGeolocation
  ]
})
export class AppModule {}
