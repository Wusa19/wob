import {Component, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';
import songtexts from "../../data/songtexts";
import {Song} from "../../data/song.interface";
import {SongtextPage} from "../songtext/songtext";


@Component({
  selector: 'page-songs',
  templateUrl: 'songs.html',
})
export class SongsPage implements OnInit {

  songs: Song[];

  constructor(private navCtrl: NavController) {

  }

  onShowSongtext(song) {
    this.navCtrl.push(SongtextPage, song);
  }

  ngOnInit(): void {
    this.songs = songtexts;

  }





}
