import { Component } from '@angular/core';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor() {
  }

  year = new Date().getFullYear();


  ionViewDidEnter() {
    //this.getCountdown();

  }

  ionViewDidLoad() {
    setInterval(() => {


      this.getCountdown();




    }, 1000)

  }
  startDate = new Date(2018, 11, 22, 12);
  now: any;
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
  startFirework: boolean = false;

  getCountdown() {
    this.now = new Date();


    this.days = this.getDays();
    this.hours = this.getHours();
    this.minutes = this.getMinutes();

    if (this.getSeconds() > 0) {
      this.seconds = this.getSeconds();
    } else {
      this.seconds = 0;
      this.minutes = 0;
      this.hours = 0;
      this.days = 0;
      this.startFirework = true;
    }
  }

  getDays() {

    let days = 0;
    switch (this.now.getMonth() + 1) {
      case 9:
        days = 30 - this.now.getDate();
        days += 31 + 30 + this.startDate.getDate();
        break;

      case 10:
        days = 31 - this.now.getDate();
        days += 30 + this.startDate.getDate();
        break;

      case 11:
        days = 30 - this.now.getDate();
        days += this.startDate.getDate();
        break;

      case 12:
        days = this.startDate.getDate() - this.now.getDate();
    }

    return days;
  }

  getHours() {
    return this.days * 24 + this.startDate.getHours() - this.now.getHours();
  }

  getMinutes() {
    return this.hours * 60 + this.startDate.getMinutes() - this.now.getMinutes() - 1;
  }

  getSeconds() {
    return this.minutes * 60 + this.startDate.getSeconds() - this.now.getSeconds() + 60;
  }

}
