import { Component } from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth'
import {TabsPage} from "../tabs/tabs";

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  user = {email: '', pwd: ''};

  constructor(private navParams: NavParams, private alertCtrl: AlertController, private navCtrl: NavController, private angularAuth: AngularFireAuth) {
    this.user.email = this.navParams.get('email');
  }


  async loginUser(user) {
    try {
      const result = await this.angularAuth.auth.signInWithEmailAndPassword(user.email, user.pwd);

      let username = this.user.email;
      let firstname = username.split('@')[0];
      firstname = firstname.charAt(0).toUpperCase() + firstname.substring(1);
      let lastname = username.split('@')[1].split('.')[0];
      lastname = lastname.charAt(0).toUpperCase() + lastname.substring(1);

      username = firstname + " " + lastname;

      this.navCtrl.setRoot(TabsPage, {
        username: username,
      })

    } catch (e) {
      this.showAlert('Error', 'Falsche Anmeldedaten!');
      console.error(e);
    }

  }

  showAlert(title: string, message: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }
}
