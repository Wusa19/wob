import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Song} from "../../data/song.interface";

/**
 * Generated class for the SongtextPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-songtext',
  templateUrl: 'songtext.html',
})
export class SongtextPage {


  song: Song;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }



  ionViewDidLoad() {
    this.song = this.navParams.data;
  }

}
