import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';

/**
 * Generated class for the ChatroomPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-chatroom',
  templateUrl: 'chatroom.html',
})
export class ChatroomPage {

  message: string = '';
  username: string = '';
  messages: object[] = [];
  s;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: AngularFireDatabase) {
    this.username = this.navParams.data;
    console.log(this.navParams.data);
    this.db.list('/chat').valueChanges().subscribe(data => {
      this.messages = data;
    })

    /*
    this.db.list('/users').push({
      username: 'Hans',
      pwd: '2222'
    })
    */

  }

  sendMessage() {
    if (this.message == '') {
      return;
    }
    this.db.list('/chat').push({
      username: this.username,
      message: this.message
    })
      .then( () => {

      })

    this.message = '';
  }

}
