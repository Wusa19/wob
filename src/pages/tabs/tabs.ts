import { Component } from '@angular/core';
import {SongsPage} from "../songs/songs";
import {MapPage} from "../map/map";
import {HomePage} from "../home/home";
import {ChatroomPage} from "../chatroom/chatroom";
import {NavParams} from "ionic-angular";


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  songsPage = SongsPage;
  mapPage = MapPage;
  homePage = HomePage;
  chatPage = ChatroomPage;
  username: String = '';

  constructor(public navParams: NavParams) {
    this.username = this.navParams.get('username');

  }
}
