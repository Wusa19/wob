import {Component, ElementRef, ViewChild} from '@angular/core';
import {AlertController, NavController, Platform} from 'ionic-angular';
import {Subscription} from "rxjs/Subscription";
import {Geolocation} from "@ionic-native/geolocation";
import { BackgroundGeolocation } from "@ionic-native/background-geolocation";
import {Storage} from "@ionic/storage";
import {filter} from "rxjs/operators";

declare var google;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  currentMapTrack = null;

  isTracking = false;
  trackedRoute = [];

  previousTracks = [];
  positionSubscription: Subscription;

  constructor(public navCtrl: NavController, private plt: Platform,
              private backgroundGeolocation: BackgroundGeolocation, private geolocation: Geolocation, private storage: Storage, private alertCtrl: AlertController) {

  }

  addStationMarker(position, info) {
    let marker = new google.maps.Marker({
      map: this.map,
      position: position,
      icon: "assets/icon/bar.png"
    });

    var infoWindow = new google.maps.InfoWindow({
      content: info
    });

    marker.addListener('click', function() {
      infoWindow.open(this.map, marker);
    });
  }

  marker = null;

  addMarker(position) {

    this.marker = new google.maps.Marker({
      map: this.map,
      position: position,
      icon: "assets/icon/santa.png"
    });

    var infoWindow = new google.maps.InfoWindow({
      content: 'Du'
    });

    this.marker.addListener('click', function() {
      infoWindow.open(this.map, this.marker);
    })
  }

  removeMarker() {
    this.marker.setMap(null);
  }

  ionViewDidLoad() {
    this.plt.ready().then(() => {
      this.loadHistoricRoutes();

      let mapOptions = {
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mypTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false
      };

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);


      let stations = [{
        name: 'Stüble',
        latlng: new google.maps.LatLng(48.702160, 10.236310)
      }, {
        name: 'Axel Grimmeisen',
        latlng: new google.maps.LatLng(48.705310, 10.235310)
      }, {
        name: 'Fam. Wegerhoff',
        latlng: new google.maps.LatLng(48.706220, 10.241040)
      }, {
        name: 'Dieter Räpple',
        latlng: new google.maps.LatLng(48.700500, 10.239720)
      }, {
        name: 'Fam. Schlumberger',
        latlng: new google.maps.LatLng(48.698600, 10.240170)
      }, {
        name: 'Fam. Baumann',
        latlng: new google.maps.LatLng(48.699290, 10.234570)
      }, {
        name: 'Fam. D\'Amore',
        latlng: new google.maps.LatLng(48.698720, 10.236510)
      }, {
        name: 'Fam. Bass',
        latlng: new google.maps.LatLng(48.696020, 10.239110)
      }, {
        name: 'Fam. Burr',
        latlng: new google.maps.LatLng(48.697990, 10.241700)
      }, {
        name: 'Fam. Hochholzner',
        latlng: new google.maps.LatLng(48.695870, 10.249270)
      }, {
        name: 'Klaus Pappe',
        latlng: new google.maps.LatLng(48.695130, 10.249910)
      }, {
        name: 'Armin Haas',
        latlng: new google.maps.LatLng(48.696730, 10.248340)
      }, {
        name: 'Fam. Schürle',
        latlng: new google.maps.LatLng(48.699770, 10.252110)
      }, {
        name: 'Fam. Kirst/Mack',
        latlng: new google.maps.LatLng(48.699400, 10.251280)
      }, {
        name: 'Hans Baamann',
        latlng: new google.maps.LatLng(48.700980, 10.251120)
      }, {
        name: 'Johann Palinkas',
        latlng: new google.maps.LatLng(48.702070, 10.250560)
      }, {
        name: 'Ringen',
        latlng: new google.maps.LatLng(48.701240, 10.238740)
      }];

      // add a marker for each station
      stations.forEach(val => {
        this.addStationMarker(val.latlng, val.name);
      });

      this.setCenter();

      // background geolocation
      let config = {
        desiredAccuracy: 0,
        stationaryRadius: 1,
        distanceFilter: 3,
        debug: false,
        interval: 2000,
        notificationsEnabled: false
      }

      this.backgroundGeolocation.configure(config).subscribe((data) => {

        setTimeout(() => {
          if (this.isTracking) {
            this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
            this.redrawPath(this.trackedRoute);
          }



          this.removeMarker();
          let latlng = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
          this.map.setCenter(latlng);
          this.addMarker(latlng);
        })

      }, (err) => {

        console.log(err);

      });

      // Turn ON the background-geolocation system.
      this.backgroundGeolocation.start();


      // foreground geolocation
      const options = {
        enableHighAccuracy: true
      }

      this.positionSubscription = this.geolocation.watchPosition(options).pipe(
        filter(p => p.coords !== undefined)
      )
        .subscribe(data => {
          setTimeout(() => {
            if (this.isTracking) {
              this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
              this.redrawPath(this.trackedRoute);
            }


            this.removeMarker();
            let latlng = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
            this.map.setCenter(latlng);
            this.addMarker(latlng);
          })
        })
    })
      .catch(e => {
        console.log(e);
      })
  }

  setCenter() {
    this.geolocation.getCurrentPosition().then(pos => {
      let latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
      this.map.setCenter(latlng);
      this.map.setZoom(15);

      this.addMarker(latlng);

    }).catch(err => {
      console.log(err);
    });
  }

  startTracking() {
    this.isTracking = true;
    this.trackedRoute = [];
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }


  }

  redrawPath(path) {
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }

    if (path.length > 1) {
      this.currentMapTrack = new google.maps.Polyline({
        path: path,
        geodisc: true,
        strokeColor: '#ff00ff',
        strokeOpacity: 1.0,
        strokeWeight: 3
      });

      this.currentMapTrack.setMap(this.map);
    }
  }

  stopTracking() {
    //let routenname = this.presentPrompt();
    let newRoute = { finished: new Date().getTime(), path: this.trackedRoute };
    this.previousTracks.push(newRoute);
    this.storage.set('routes', this.previousTracks);

    this.isTracking = false;
    //this.positionSubscription.unsubscribe();
    //this.backgroundGeolocation.finish();
    this.currentMapTrack.setMap(null);
    this.loadHistoricRoutes();
  }

  deleteRoute(route) {
    let timestamp = route.finished;

    this.previousTracks = this.previousTracks.filter((val, idx, arr) => {
      return val.finished !== timestamp;
    });

    this.storage.set('routes', this.previousTracks);
  }

  showHistoryRoute(route) {
    this.redrawPath(route);
  }

  loadHistoricRoutes() {
    this.storage.get('routes').then((data) => {
      if (data) {
        this.previousTracks = data;
      }
    })
      .catch(err => {
        console.log(err);
      })
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Name für die Route',
      inputs: [
        {
          name: 'Routenname',
          placeholder: 'Routenname'
        }
      ],
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Hinzufügen',
          handler: data => {
            return data;
          }
        }
      ]
    });
    alert.present();
  }

  removeAllPreviousRoutes() {
    this.currentMapTrack.setMap(null);
  }
}
